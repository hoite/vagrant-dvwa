# Vagrant DVWA

DVWA box voor Virtualbox

## Installatie

```
git clone https://gitlab.com/hoite/vagrant-dvwa
cd vagrant-dvwa
vagrant up
```

## Gebruik
Open een browser en ga naar http://127.0.0.1:8080

Credentials van DVWA:
`admin:password`
